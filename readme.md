# Orion

[![MicroBadger Size](https://img.shields.io/microbadger/image-size/fspnetwork/orion.svg?style=for-the-badge)](https://microbadger.com/#/images/fspnetwork/orion)
[![Docker Automated build](https://img.shields.io/docker/automated/fspnetwork/orion.svg?style=for-the-badge)](https://hub.docker.com/r/fspnetwork/orion/)
[![Docker Build Status](https://img.shields.io/docker/build/fspnetwork/orion.svg?style=for-the-badge)](https://hub.docker.com/r/fspnetwork/orion/)
[![GitHub](https://img.shields.io/github/license/fspnet/orion.svg?style=for-the-badge)](https://github.com/fspnet/orion/blob/master/LICENSE)
[![Release version](https://img.shields.io/github/release/fspnet/orion.svg?style=for-the-badge)](https://github.com/FSPNet/Orion/releases)

## About Orion

Orion is a web application that can get game server configuration files online. We hope that we no longer have to deal with configuration files in various formats, we only need to edit them with Orion. Orion based Laravel.

## License

[AGPLv3](LICENSE)
